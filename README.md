## Bitbucket Repository
https://bitbucket.org/ManukMinasyan1/laravel-skills-test-v1.0/

## Install

Via Git Clone
``` 
git clone https://ManukMinasyan1@bitbucket.org/ManukMinasyan1/laravel-skills-test-v1.0.git
```
Update Composer
```
php composer.phar update
```