<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ProductController@index')->name('index');
Route::get('/get-all', 'ProductController@get');
Route::post('/add-new', 'ProductController@add');
Route::put('/edit/{product_index}', 'ProductController@edit');
Route::delete('/delete/{product_index}', 'ProductController@delete');