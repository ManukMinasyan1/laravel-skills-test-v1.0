<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function get()
    {
        $productsExists = Storage::disk('public')->exists('products.json');
        $products = $productsExists ? json_decode(Storage::disk('public')->get('products.json')) : [];

        return response()->json($products);
    }

    public function add(Request $request)
    {
        $data = $request->except('_token');

        $productsExists = Storage::disk('public')->exists('products.json');
        $products = $productsExists ? json_decode(Storage::disk('public')->get('products.json')) : [];
        $data['created_at'] = date('Y-m-d H:i:s');

        array_push($products, $data);

        Storage::disk('public')->put('products.json', json_encode($products));

        return response()->json(['success'=>true]);
    }

    public function edit(Request $request, $product_index)
    {
        $data = $request->except('_token');
        $products = json_decode(Storage::disk('public')->get('products.json'));
        $products[$product_index] = $data;
        Storage::disk('public')->put('products.json', json_encode($products));

        return response()->json(['success'=>true]);
    }

    public function delete($product_index) {
        $products = json_decode(Storage::disk('public')->get('products.json'));
        $products = array_except($products, $product_index);
        Storage::disk('public')->put('products.json', json_encode($products));

        return response()->json(['success'=>true]);
    }
}
